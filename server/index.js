// const express = require('express');
// const app = express();
// const mongoose = require("mongoose");
// const dotenv = require("dotenv");
// const path = require('path');

// dotenv.config({path:"./.env"})
// require('./db/conn');
// const  User = require('./model/userSchema');

// app.use(express.json());


// const PORT = process.env.PORT || 3000;


// app.get('/', (req, res) => {
//     res.send('Hello world from  server');
// });

// app.get('/mess', (req, res) => {
//     res.sendFile(path.join(__dirname, '/mess.html'));
// });

// app.post('/sendmessage', (req, res)=>{
//     console.log("hi1");
//     console.log(req.body);
//     const {name, email, subject, message} = req.body;

//     try
//     {
//         console.log(req.body);
//         console.log("hi1");
//         const user = new User({name, email, subject, message});   
//         console.log("hi1");
//         const MessageRegistered = user.save();

//         if(MessageRegistered)
//         {
//             return res.status(200).json("User registered successfully");
//         }
//     } catch (err)
//     {
//         console.log(err);
//     }
// });

// app.listen(PORT, ()=>{
//     console.log(`successful at port ${PORT}`);
// });


var express=require("express");
const mongoose=require('mongoose');
var bodyParser=require("body-parser");
const dotenv = require("dotenv");
const path = require('path');

dotenv.config({path:"./.env"})

const DB = process.env.MONGO_URL;

mongoose.connect(DB, {
    useNewUrlParser:true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify:false
}).then(()=>{
    console.log("connection successful");
}).catch((err)=>console.log(err));
var db=mongoose.connection;
var app=express()
  
  
app.use(bodyParser.json());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
    extended: true
}));


app.get('/mess', (req, res) => {
    res.sendFile(path.join(__dirname, 'mess.html'));
});
  
app.post('/sendmessage', function(req,res){
    var name = req.body.name;
    var email =req.body.email;
    var subject = req.body.subject;
    var message =req.body.message;
  
    var data = {
        "name": name,
        "email":email,
        "subject":subject,
        "message":message
    }

db.collection('Users').insertOne(data,function(err, collection){
        if (err) throw err;
        console.log("Record inserted Successfully");
        res.send("hiii");
    });
          
    return res.redirect('mess.html');
})
  
  
app.get('/',function(req,res){
    res.send('Hello world from  server');
}).listen(3000)
  
  
console.log("server listening at port 3000");




// var express = require('express');
// var path = require('path');
// var bodyParser = require('body-parser');
// var mongodb = require('mongodb');

// var dbConn = mongodb.MongoClient.connect('mongodb://localhost:27017');

// var app = express();

// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(express.static(path.resolve(__dirname, 'public')));

// app.post('/post-feedback', function (req, res) {
//     dbConn.then(function(db) {
//         delete req.body._id; // for safety reasons
//         db.collection('feedbacks').insertOne(req.body);
//     });    
//     res.send('Data received:\n' + JSON.stringify(req.body));
// });

// app.get('/view-feedbacks',  function(req, res) {
//     dbConn.then(function(db) {
//         db.collection('feedbacks').find({}).toArray().then(function(feedbacks) {
//             res.status(200).json(feedbacks);
//         });
//     });
// });

// app.listen(process.env.PORT || 3000, process.env.IP || '0.0.0.0' );