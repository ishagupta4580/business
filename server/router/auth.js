const express = require("express");
const router = express.Router();
// const bcrypt = require("bcryptjs");
// const jwt = require("jsonwebtoken");

require('../db/conn');
const User = require('../model/userSchema');

router.get('/', (req, res)=>{
    res.send("hello from server");
});

// using promisess
// router.post('/register', (req, res)=>{
//     const {name, email, phone, work, password, cpassword} = req.body;
//     // console.log(name);
//     // console.log(email);
//     // res.json({message: req.body});

//     if(!name || !email || !phone || !work || !password || !cpassword)
//     {
//         return res.status(422).json({error: "plz fill"});
//     }

//     User.findOne({ email: email })
//     .then((userExists)=>
//     {
//         if(userExists)
//         {
//             return res.status(422).json("User already exists");
//         }
//         const user = new User({name, email, phone, work, password, cpassword});
//         user.save()
//         .then(()=>
//         {
//             res.status(200).json("User registered successfully");
//         }).catch((err)=>{res.status(500).json({error:"failed registration"})});
//     }).catch((err)=>{ console.log(err); });
// });


// using async await
// router.post('/register', async (req, res)=>{
//     const {name, email, phone, work, password, cpassword} = req.body;
//     // console.log(name);
//     // console.log(email);
//     // res.json({message: req.body});

//     if(!name || !email || !phone || !work || !password || !cpassword)
//     {
//         return res.status(422).json({error: "plz fill"});
//     }

//     try
//     {
//         const userExists = await User.findOne({ email: email });
//         if(userExists)
//         {
//             return res.status(422).json("User already exists");
//         }
//         else if(password != cpassword)
//         {
//             return res.status(422).json("Password not matching");
//         }
//         else
//         {
//             const user = new User({name, email, phone, work, password, cpassword});   

//             const userRegistered = await user.save();

//             if(userRegistered)
//             {
//                 return res.status(200).json("User registered successfully");
//             }
//             else
//             {
//                 return res.status(500).json({error:"failed registration"});
//             }
//             }
//     } catch (err)
//     {
//         console.log(err);
//     }
// });


// router.post('/signin', async (req, res) => {
//     // console.log(req.body);
//     try
//     {
//         let token;
//         const {email, password}=req.body;
//         if(!email || !password)
//         {
//             return res.status(400).json({error: "plz fill data"});
//         }

//         const userLogin = await User.findOne({email:email});
//         if(userLogin)
//         {
//             const isMatch = await bcrypt.compare(password, userLogin.password);
//             token = await userLogin.generateAuthToken();
//             res.cookie("jwtoken", token, {
//                 expires: new Date(Date.now() + 25892000000),
//                 httpOnly: true
//             });

//             if(!isMatch)
//             {
//                 res.status(400).json({error: "Invalid Credentials"});
//             }
//             else
//             {
//                 res.status(200).json({message:"user signin successfully"});
//             }
//         }
//         else
//         {
//             res.status(400).json({error: "Invalid Credentials"});
//         }
//     }
//     catch(err)
//     {
//         console.log(err);
//     }
// });

module.exports = router;